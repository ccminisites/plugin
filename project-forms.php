<?php

add_action('rest_api_init', function () {
    register_rest_route('project/v1', '/form', [
        'methods' => 'POST',
        'callback' => 'project_form',
        'permission_callback' => function () {
            return true;
        },
    ]);
    register_rest_route('project/v2', '/form', [
      'methods' => 'POST',
      'callback' => 'project_form_vii',
      'permission_callback' => function () {
          return true;
      },
    ]);
    register_rest_route('project/v2', '/form-export', [
      'methods' => 'POST',
      'callback' => 'project_form_export',
      'permission_callback' => function (WP_REST_Request $request) {
        $nonce = wp_verify_nonce($request->get_param('_wpnonce'), 'wp_rest');
        $user = wp_get_current_user();
        return in_array('administrator', $user->roles);
      },
    ]);
    register_rest_route('project/v2', '/form-clear', [
      'methods' => 'POST',
      'callback' => 'project_form_clear',
      'permission_callback' => function (WP_REST_Request $request) {
          $nonce = wp_verify_nonce($request->get_param('_wpnonce'), 'wp_rest');
          $user = wp_get_current_user();
          return in_array('administrator', $user->roles);
      },
    ]);
});


function project_form(WP_REST_Request $request)
{
    // You can get the combined, merged set of parameters:
    $parameters = $request->get_params();

    // Lets take from the form just what we need.
    $dataDesired = [];

    $dataDesired['contact'] = [
        'fullname',
        'company',
        'phone',
        'email',
        'locale',
        'form_id',
        'received',
        'message',
    ];

    $dataDesired['newsletter'] = [
        'email',
        'locale',
        'form_id',
    ];

    $dataDesired = apply_filters('project_form_data_desired', $dataDesired);
    // Allowed form ids to be sent in.
    $form_ids = array_keys($dataDesired);

    // Every form has to have an ID, Email and Token.
    // The ID has to be in the above array, Email valid, and the token has to be human.
    if (isset($parameters['form_id'], $parameters['email']) &&
        in_array($parameters['form_id'], $form_ids, true) &&
        filter_var($parameters['email'], FILTER_VALIDATE_EMAIL)
    ) {

        $received = wp_date('Y-m-d H:i:s');
        $parameters['received'] = $received;



        // Start a blank string for putting in the message later.
        $messageData = '';

        foreach ($dataDesired[$parameters['form_id']] as $key) {
            if (isset($parameters[$key]) && !empty($parameters[$key])) {
                if (is_array($parameters[$key])) {
                    $parameters[$key] = implode(", ", $parameters[$key]);
                }
                // Nothing longer than 500 bytes, even then still...
                $postMeta[$key] = substr($parameters[$key], 0, 50000);
            } else {
                $postMeta[$key] = '';
            }
            $messageData .= str_pad($key, 20, ' ') . ': ' . $postMeta[$key] . "\n";
        }

        $hostname = $_SERVER['HTTP_HOST'];

        $message = "Team,\n\nA user filled in the " . $parameters['form_id'] . " form on the $hostname website.\n\n";
        $message .= "Here is their data:\n\n";
        $message .= "```\n";
        $message .= $messageData;
        $message .= "```";
        $message .= "\n\nWe hope that you are having a great day!";
        $message .= "\n\n- The Website";


        // Fake a comment and check it on the askimet.
        $content = [];
        $content['comment_author'] = $parameters['fullname'] ?? 'Cleveland Booker';
        $content['comment_author_email'] = $parameters['email'];
        $content['comment_content'] = $parameters['message'] ?? 'Hi there thanks for dinner last night';

        /*if (!project_check_spam($content)) {*/
            pll__('form-first-email');
            $to = pll_translate_string('form-first-email', 'en');
            if (filter_var($to, FILTER_VALIDATE_EMAIL)) {
                wp_mail($to, "Form Entry: " . $parameters['form_id'], $message);
            }

            pll__('form-second-email');
            $to = pll_translate_string('form-second-email', 'en');
            if (filter_var($to, FILTER_VALIDATE_EMAIL)) {
                wp_mail($to, "Form Entry: " . $parameters['form_id'], $message);
            }

            pll__('form-third-email');
            $to = pll_translate_string('form-third-email', 'en');
            if (filter_var($to, FILTER_VALIDATE_EMAIL)) {
                wp_mail($to, "Form Entry: " . $parameters['form_id'], $message);
            }
        /*} else {
            $message .= "\n\n--- This message was considered as spam by AKISMET and was not sent to the email addresses. ---";
            $to = pll__('form-spam-email');
            die('<pre>' . print_r($to, true) . '</pre>');
            if (filter_var($to, FILTER_VALIDATE_EMAIL)) {
                wp_mail($to, "Form Entry: " . $parameters['form_id'], $message);
            }
        }*/
        return "OK";
    }

    return "NOK";
}

function project_form_vii(WP_REST_Request $request)
{
    // You can get the combined, merged set of parameters:
    $parameters = $request->get_params();

    if (!isset($parameters['form_id'])) {
        return 'NOK';
    }

    $form = get_post((int)$parameters['form_id']);

    if (!$form || $form->post_type !== 'form') {
        return 'NOK';
    }

    if (!isset($parameters['locale'])) {
        return 'NOK';
    }

    $received = wp_date('Y-m-d H:i:s');

    $postMeta = [];
    $postMeta['received'] = $received;
    $postMeta['form_id'] = $form->ID;
    $postMeta['form_title'] = $form->post_title;
    $postMeta['locale'] = $parameters['locale'];

    $messageData = '';
    $formData = get_field('the_form', $form->ID);

    $formFields = json_decode(preg_replace('/"$/', '', preg_replace('/^\"/', '', stripslashes($formData))), true);


    foreach ($formFields as $formField) {
        if (
          isset($formField['name']) &&
          !(
            $formField['type'] == 'button' &&
            $formField['subtype'] == 'submit'
          )
        ) {
            $key = $formField['name'];

            if (
              isset($parameters[$key]) &&
              !empty($parameters[$key])
            ) {
                if (is_array($parameters[$key])) {
                    $parameters[$key] = implode(", ", $parameters[$key]);
                }
                // Nothing longer than 500 chars, even then still...
                $postMeta[$key] = substr($parameters[$key], 0, 50000);
            } else {
                $postMeta[$key] = '';
            }

            $messageData .= str_pad($key, 20, ' ') . ': ' . $postMeta[$key] . "\n";
        }
    }
    $messageData .= str_pad('received', 20, ' ') . ': ' . $received . "\n";

    $hostname = $_SERVER['HTTP_HOST'];

    $message = "Team,\n\nA user filled in the " . $parameters['form_id'] . " form on the $hostname website.\n\n";
    $message .= "Here is their data:\n\n";
    $message .= "```\n";
    $message .= $messageData;
    $message .= "```";
    $message .= "\n\nWe hope that you are having a great day!";
    $message .= "\n\n- The Website";

    $to = get_field('email_responses_to', $form->ID);
    if ($to && filter_var($to, FILTER_VALIDATE_EMAIL)) {
        wp_mail($to, "Form Response: " . $form->post_title, $message);
    }

    $formResponseId = wp_insert_post([
      'post_type' => 'form-response',
      'post_title' => 'Response: ' . $form->post_title . ' ('.$received.')',
      'post_status' => 'publish'
    ]);

    foreach ($postMeta as $key => $value) {
        update_post_meta($formResponseId, $key, $value);
    }

    return "OK";
}


function project_form_export(WP_REST_Request $request)
{
    // You can get the combined, merged set of parameters:
    $parameters = $request->get_params();

    if (!isset($parameters['form_id'])) {
        return 'NOK';
    }

    $form = get_post((int)$parameters['form_id']);

    if (!$form || $form->post_type !== 'form') {
        return 'NOK';
    }

    $formId = $request->get_param('form_id');
    $form = get_post($formId);

    if ($form && $form->post_type == 'form') {

        $args = [
          'post_status' => 'publish',
          'posts_per_page' => -1,
          'post_type' => 'form-response',
          'meta_key' => 'form_id',
          'meta_value' => $form->ID
        ];
        $responses = new WP_Query($args);

        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=".str_replace(" ", "-", mb_strtolower($form->post_title))."-export.csv");

        $first = true;
        $out = fopen('php://output', 'w');
        while ($responses->have_posts()) {
            $responses->the_post();
            $metaData = get_post_meta(get_the_ID());
            if ($first) {
                fputcsv($out, array_keys($metaData));
                $first = false;
            }
            fputcsv($out, array_map(function($item) {
                return $item[0];
            },$metaData));
        }
        fclose($out);

        exit(0);
    }

    return "Error";
}

function project_form_clear(WP_REST_Request $request)
{
    // You can get the combined, merged set of parameters:
    $parameters = $request->get_params();

    if (!isset($parameters['form_id'])) {
        return 'NOK';
    }

    $form = get_post((int)$parameters['form_id']);

    if (!$form || $form->post_type !== 'form') {
        return 'NOK';
    }

    $formId = $request->get_param('form_id');
    $form = get_post($formId);

    if ($form && $form->post_type == 'form') {

        $args = [
          'post_status' => 'publish',
          'posts_per_page' => -1,
          'post_type' => 'form-response',
          'meta_key' => 'form_id',
          'meta_value' => $form->ID
        ];
        $responses = new WP_Query($args);

        $first = true;
        $out = fopen('php://output', 'w');
        while ($responses->have_posts()) {
            $responses->the_post();
            $metaData = get_post_meta(get_the_ID());
            $metaKeys = array_keys($metaData);
            foreach ($metaKeys as $metaKey) {
                delete_post_meta(get_the_ID(), $metaKey);
            }
            wp_delete_post(get_the_ID(), true);

        }
        header("Location: /wp-admin/edit.php?post_type=form");
        exit(0);
    }

    return "Error";
}

add_filter( 'manage_form_posts_columns', 'project_filter_posts_columns' );
function project_filter_posts_columns( $columns ) {
    $columns['responses'] = __( 'Responses', 'project' );
    $columns['export'] = __( 'Export', 'project' );
    $columns['clear'] = __( 'Clear Responses', 'project' );
    return $columns;
}

add_action( 'manage_form_posts_custom_column', 'project_form_column', 10, 2);
function project_form_column( $column, $post_id ) {
    global $wpdb;

    if ( 'responses' === $column ) {
        $count = $wpdb->get_row("SELECT COUNT(*) AS THE_COUNT FROM $wpdb->postmeta WHERE (meta_key = 'form_id' AND meta_value = ".$post_id.")");
        echo $count->THE_COUNT;
    }

    if ( 'export' === $column ) {
        echo '<form method="post" action="'.get_home_url().'/wp-json/project/v2/form-export"><input type="hidden" name="_wpnonce" value="'.wp_create_nonce( 'wp_rest' ).'" /><input type="hidden" name="form_id" value="'.$post_id.'" /><input type="submit" class="button" value="export" /></form>';
    }

    if ( 'clear' === $column ) {
        echo '<form method="post" action="'.get_home_url().'/wp-json/project/v2/form-clear"><input type="hidden" name="_wpnonce" value="'.wp_create_nonce( 'wp_rest' ).'" /><input type="hidden" name="form_id" value="'.$post_id.'" /><input type="submit" class="button" value="clear" onClick="return confirm(\'Are you really sure you want to delete the responses to this form? This can not be undone!\')"/></form>';
    }
}
