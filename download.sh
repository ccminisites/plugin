# OK go get the repo as a zipo.
rm -rf project
wget -q https://gitlab.com/ccminisites/plugin/-/archive/master/plugin-master.zip
unzip plugin-master.zip
rm -rf plugin-master.zip
mv plugin-master project
rm -rf hide-my-site
mv project/hide-my-site ./
rm project/download.sh
rm project/README.md
mv project/README-GENERIC.md project/README.md
