<?php

// create custom plugin settings menu
add_action('admin_menu', 'project_create_menu');

function project_create_menu()
{
    //create new top-level menu
    add_menu_page('Project Settings', 'Project Settings', 'administrator', __FILE__, 'project_settings_page', 'dashicons-awards');

    //call register settings function
    add_action('admin_init', 'project_register_project_settings');
}


function project_register_project_settings()
{
    //register our settings
    register_setting('project-settings-group', 'rm_id');
    register_setting('project-settings-group', 'rm_username');
    register_setting('project-settings-group', 'rm_password');
    register_setting('project-settings-group', 'p_body_color');
    register_setting('project-settings-group', 'p_primary_font_color');
    register_setting('project-settings-group', 'p_secondary_font_color');
    register_setting('project-settings-group', 'p_primary_color');
    register_setting('project-settings-group', 'p_secondary_color');
    register_setting('project-settings-group', 'p_supporting_color');
    register_setting('project-settings-group', 'p_primary_link_color');
    register_setting('project-settings-group', 'p_primary_link_hover_color');
    register_setting('project-settings-group', 'p_secondary_link_color');
    register_setting('project-settings-group', 'p_secondary_link_hover_color');
    register_setting('project-settings-group', 'p_logo', 'handle_p_logo_upload');
    register_setting('project-settings-group', 'p_logo_flyout', 'handle_p_logo_flyout_upload');
    register_setting('project-settings-group', 'p_logo_footer', 'handle_p_logo_footer_upload');
    register_setting('project-settings-group', 'p_logo_height');
    register_setting('project-settings-group', 'p_logo_height_mobile');
    register_setting('project-settings-group', 'p_logo_flyout_height');
    register_setting('project-settings-group', 'p_logo_flyout_height_mobile');
    register_setting('project-settings-group', 'p_logo_footer_height');
    register_setting('project-settings-group', 'p_logo_footer_height_mobile');
    register_setting('project-settings-group', 'p_menu_variant');
    register_setting('project-settings-group', 'p_form_variant');
    register_setting('project-settings-group', 'p_socials_in_menu');
    register_setting('project-settings-group', 'p_border_radius');
    register_setting('project-settings-group', 'p_secondary_font');

    register_setting('project-settings-group', 'p_title');
    register_setting('project-settings-group', 'p_description');
    register_setting('project-settings-group', 'p_gmap');
    register_setting('project-settings-group', 'p_human_address');
    register_setting('project-settings-group', 'p_email');
    register_setting('project-settings-group', 'p_phone');
    register_setting('project-settings-group', 'p_human_phone');
    register_setting('project-settings-group', 'p_hours');

    register_setting('project-settings-group', 'p_instagram_link');
    register_setting('project-settings-group', 'p_facebook_link');
    register_setting('project-settings-group', 'p_linkedin_link');
    register_setting('project-settings-group', 'p_x-twitter_link');
    register_setting('project-settings-group', 'p_youtube_link');

    register_setting('project-settings-group', 'p_google_tag_manager');

    register_setting('project-settings-group', 'p_header_raw');
    register_setting('project-settings-group', 'p_footer_raw');


    register_setting('project-settings-group', 'p_parkit_on');
    register_setting('project-settings-group', 'p_parkit_card');
    register_setting('project-settings-group', 'p_parkit_image', 'handle_p_parkit_image');


    register_setting('project-settings-group', 'p_branding_bar_show');

}

function project_parkit_cards()
{

    $files = glob(get_template_directory() . "/parkit/*.php");
    $cards = [];
    foreach ($files as $file) {
        $base = basename($file, ".php");
        $cards[$base] = $file;
    }
    ksort($cards);
    return $cards;
}



function handle_p_upload($option) {
    if (isset($_POST['delete_' . $option]) && $_POST['delete_' . $option] === 'remove') {
        return '';
    }
    if ( ! empty($_FILES[$option]["tmp_name"])) {
        $urls = wp_handle_upload($_FILES[$option], array('test_form' => false));

        return $urls["url"];
    }
    return get_option($option);
}

function handle_p_logo_upload()
{
    return handle_p_upload('p_logo');
}

function handle_p_logo_flyout_upload()
{
    return handle_p_upload('p_logo_flyout');
}

function handle_p_logo_footer_upload()
{
    return handle_p_upload('p_logo_footer');
}

function handle_p_parkit_image()
{
    return handle_p_upload('p_parkit_image');
}


function project_settings_page()
{
    $fields = [];

    ?>
  <div class="wrap">
    <h1>Project Settings</h1>

    <!-- Tab links -->
    <div class="tab">
      <button class="tablinks" onclick="openSettings('Branding')" id="tab-Branding">Branding</button>
      <button class="tablinks" onclick="openSettings('Menus')" id="tab-Menus">Menus</button>
      <button class="tablinks" onclick="openSettings('Forms')" id="tab-Forms">Forms</button>
      <button class="tablinks" onclick="openSettings('Extra')" id="tab-Extra">Extra</button>
      <button class="tablinks" onclick="openSettings('Social')" id="tab-Social">Meta & Social</button>
      <button class="tablinks" onclick="openSettings('SearchAnalytics')" id="tab-Social">Search & Analytics</button>
      <button class="tablinks" onclick="openSettings('Parkit')" id="tab-Parkit">Parkit</button>
        <button class="tablinks" onclick="openSettings('BrandingFooter')" id="tab-BrandingFooter">Branding Footer</button>
    </div>
    <form method="post" action="options.php" enctype="multipart/form-data">
        <?php
        settings_fields('project-settings-group'); ?>
        <?php
        do_settings_sections('project-settings-group'); ?>
      <!-- Tab content -->
      <div id="Branding" class="tabcontent">
        <table class="form-table">
          <tr>
            <th scope="row">Main body Color</th>
            <td><input type="text" name="p_body_color" value="<?php
                echo esc_attr(get_option('p_body_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Primary Font Color</th>
            <td><input type="text" name="p_primary_font_color" value="<?php
                echo esc_attr(get_option('p_primary_font_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Secondary Font Color</th>
            <td><input type="text" name="p_secondary_font_color" value="<?php
                echo esc_attr(get_option('p_secondary_font_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Primary Color</th>
            <td><input type="text" name="p_primary_color" value="<?php
                echo esc_attr(get_option('p_primary_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Secondary Color</th>
            <td><input type="text" name="p_secondary_color" value="<?php
                echo esc_attr(get_option('p_secondary_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Supporting Color</th>
            <td><input type="text" name="p_supporting_color" value="<?php
                echo esc_attr(get_option('p_supporting_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Primary Link Color</th>
            <td><input type="text" name="p_primary_link_color" value="<?php
                echo esc_attr(get_option('p_primary_link_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Primary Link Color On Hover</th>
            <td><input type="text" name="p_primary_link_hover_color" value="<?php
                echo esc_attr(get_option('p_primary_link_hover_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Secondary Link Color</th>
            <td><input type="text" name="p_secondary_link_color" value="<?php
                echo esc_attr(get_option('p_secondary_link_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Secondary Link Color On Hover</th>
            <td><input type="text" name="p_secondary_link_hover_color" value="<?php
                echo esc_attr(get_option('p_secondary_link_hover_color')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Logo</th>
            <td><input type="file" name="p_logo"/>
              <br/>
                <?php
                echo esc_attr(get_option('p_logo')); ?>
              <br/>
              <input type="checkbox" name="delete_p_logo" value="remove"/>clear?
            </td>
          </tr>
            <tr>
              <th rowspan="2" scope="row">Logo menu height</th>
              <td>Desktop size: <input type="number" step="0.01" name="p_logo_height" value="<?php
                  echo esc_attr(get_option('p_logo_height')); ?>"/> px
              </td>
                <tr>
              <td>Mobile size: <input type="number" step="0.01" name="p_logo_height_mobile" value="<?php
                  echo esc_attr(get_option('p_logo_height_mobile')); ?>"/> px
              </td>
            </tr>
          </tr>
          <tr>
            <th scope="row">Logo Flyout</th>
            <td><input type="file" name="p_logo_flyout"/>
              <br/>
                <?php
                echo esc_attr(get_option('p_logo_flyout')); ?>
              <br/>
              <input type="checkbox" name="delete_p_logo_flyout" value="remove"/>clear?
            </td>
          </tr>
            <tr>
              <th rowspan="2" scope="row">Logo Flyout height</th>
              <td>Desktop size: <input type="number" step="0.01" name="p_logo_flyout_height" value="<?php
                  echo esc_attr(get_option('p_logo_flyout_height')); ?>"/> px
              </td>
                <tr>
              <td>Mobile size: <input type="number" step="0.01" name="p_logo_flyout_height_mobile" value="<?php
                  echo esc_attr(get_option('p_logo_flyout_height_mobile')); ?>"/> px
              </td>
            </tr>
          </tr>
          <tr>
            <th scope="row">Logo footer</th>
            <td><input type="file" name="p_logo_footer"/>
              <br/>
                <?php
                echo esc_attr(get_option('p_logo_footer')); ?>
              <br/>
              <input type="checkbox" name="delete_p_logo_footer" value="remove"/>clear?
            </td>
          </tr>
            <tr>
              <th rowspan="2" scope="row">Logo footer height</th>
              <td>Desktop size: <input type="number" step="0.01" name="p_logo_footer_height" value="<?php
                  echo esc_attr(get_option('p_logo_footer_height')); ?>"/> px
              </td>
              <tr>
              <td>Mobile size: <input type="number" step="0.01" name="p_logo_footer_height_mobile" value="<?php
                  echo esc_attr(get_option('p_logo_footer_height_mobile')); ?>"/> px
              </td>
            </tr>
          </tr>
        </table>
      </div>

      <div id="Menus" class="tabcontent">
        <table class="form-table">
          <tr>
            <th>Menu variant</th>
            <td>
              <select id="menu_variant" name="p_menu_variant"><?php
                  echo esc_attr(get_option('p_menu_variant')); ?>
                <option value="default" <?= get_option('p_menu_variant') === 'default' ? 'SELECTED' : '' ?>>Default</option>
                <option value="flyout_small" <?= get_option('p_menu_variant') === 'flyout_small' ? 'SELECTED' : '' ?>>Flyout small</option>
                <option value="flyout_large" <?= get_option('p_menu_variant') === 'flyout_large' ? 'SELECTED' : '' ?>>Flyout large</option>
              </select>
            </td>
          </tr>
          <tr>
            <th>Show socials in menu</th>
            <td>
              <select id="socials_in_menu" name="p_socials_in_menu"><?php
                  echo esc_attr(get_option('p_socials_in_menu')); ?>
                <option value="yes" <?= get_option('p_socials_in_menu') === 'yes' ? 'SELECTED' : '' ?>>Yes</option>
                <option value="no" <?= get_option('p_socials_in_menu') === 'no' ? 'SELECTED' : '' ?>>No</option>
              </select>
            </td>
          </tr>

        </table>
      </div>

      <div id="Forms" class="tabcontent">
        <table class="form-table">
          <tr>
            <th>Form variant</th>
            <td>
              <select id="form_variant" name="p_form_variant"><?php
                  echo esc_attr(get_option('p_form_variant')); ?>
                <option value="form-regular" <?= get_option('p_form_variant') === 'regular' ? 'SELECTED' : '' ?>>Regular</option>
                <option value="bottom-bordered" <?= get_option('p_form_variant') === 'bottom_bordered' ? 'SELECTED' : '' ?>>Bottom Bordered</option>
              </select>
            </td>
          </tr>
        </table>
      </div>

      <div id="Extra" class="tabcontent">
        <table class="form-table">
          <tr>
            <th scope="row">Border Radius</th>
            <td><input type="number" step="0.01" name="p_border_radius" value="<?php
                echo esc_attr(get_option('p_border_radius')); ?>"/> rem
            </td>
          </tr>
          <tr>
            <th scope="row">Secondary Font</th>
            <td>
              <select id="secondary_font_variant" name="p_secondary_font"><?php
                  echo esc_attr(get_option('p_secondary_font')); ?>
                <option value="'Nunito', Helvetica, Arial, sans-serif" <?= get_option('p_secondary_font') === "'Nunito', Helvetica, Arial, sans-serif" ? 'SELECTED' : '' ?>>'Nunito', Helvetica, Arial, sans-serif</option>
                <option value="'Montserrat', Helvetica, Arial, sans-serif" <?= get_option('p_secondary_font') === "'Montserrat', Helvetica, Arial, sans-serif" ? 'SELECTED' : '' ?>>'Montserrat', Helvetica, Arial, sans-serif</option>
                <option value="'Roboto Slab', Helvetica, Arial, sans-serif" <?= get_option('p_secondary_font') === "'Roboto Slab', Helvetica, Arial, sans-serif" ? 'SELECTED' : '' ?>>'Roboto Slab', Helvetica, Arial, sans-serif</option>
                <option value="'Inter', Helvetica, Arial, sans-serif" <?= get_option('p_secondary_font') === "'Inter', Helvetica, Arial, sans-serif" ? 'SELECTED' : '' ?>>'Inter', Helvetica, Arial, sans-serif</option>
              </select>
            </td>
          </tr>
          <tr>
            <th scope="row">Raw Header HTML</th>
            <td><textarea name="p_header_raw" rows="10" cols="80"><?php echo esc_attr(get_option('p_header_raw')); ?></textarea></td>
          </tr>
          <tr>
            <th scope="row">Raw Footer HTML</th>
            <td><textarea name="p_footer_raw" rows="10" cols="80"><?php echo esc_attr(get_option('p_footer_raw')); ?></textarea></td>
          </tr>
        </table>
      </div>

      <div id="Social" class="tabcontent">
        <table class="form-table">

          <tr>
            <th scope="row">Project Title</th>
            <td><input type="text" name="p_title" value="<?php
                echo esc_attr(get_option('p_title')); ?>"/></td>
          </tr>

          <tr>
            <th scope="row">Project Description</th>
            <td><textarea name="p_description" cols="80"><?php
                    echo esc_attr(get_option('p_description')); ?></textarea></td>
          </tr>

          <tr>
            <th scope="row">Project GMAP</th>
            <td><input type="text" name="p_gmap" value="<?php
                echo esc_attr(get_option('p_gmap')); ?>"/></td>
          </tr>

          <tr>
            <th scope="row">Project Human Address</th>
            <td><input type="text" name="p_human_address" value="<?php
                echo esc_attr(get_option('p_human_address')); ?>"/></td>
          </tr>


          <tr>
            <th scope="row">Project Email Address</th>
            <td><input type="text" name="p_email" value="<?php
                echo esc_attr(get_option('p_email')); ?>"/></td>
          </tr>


          <tr>
            <th scope="row">Project Phone</th>
            <td><input type="text" name="p_phone" value="<?php
                echo esc_attr(get_option('p_phone')); ?>"/></td>
          </tr>

          <tr>
            <th scope="row">Project Human Phone</th>
            <td><input type="text" name="p_human_phone" value="<?php
                echo esc_attr(get_option('p_human_phone')); ?>"/></td>
          </tr>

          <tr>
            <th scope="row">Project Open Hours</th>
            <td><textarea name="p_hours" rows="5" cols="80"><?php
                    echo esc_attr(get_option('p_hours')); ?></textarea></td>
          </tr>


          <tr>
            <th scope="row">Instagram Link</th>
            <td><input type="text" name="p_instagram_link" value="<?php
                echo esc_attr(get_option('p_instagram_link')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Facebook Link</th>
            <td><input type="text" name="p_facebook_link" value="<?php
                echo esc_attr(get_option('p_facebook_link')); ?>"/></td>
          </tr>
          <tr>
            <th scope="row">Linkedin Link</th>
            <td><input type="text" name="p_linkedin_link" value="<?php
                echo esc_attr(get_option('p_linkedin_link')); ?>"/></td>
          </tr>
            <tr>
                <th scope="row">Twitter Link</th>
                <td><input type="text" name="p_x-twitter_link" value="<?php
                    echo esc_attr(get_option('p_x-twitter_link')); ?>"/></td>
            </tr>
            <tr>
                <th scope="row">Youtube Link</th>
                <td><input type="text" name="p_youtube_link" value="<?php
                    echo esc_attr(get_option('p_youtube_link')); ?>"/></td>
            </tr>
        </table>
      </div>

      <div id="SearchAnalytics" class="tabcontent">
        <table class="form-table">
          <tr>
            <th scope="row">Google Tag Manger Id</th>
            <td>
                GTM-<input type="text" name="p_google_tag_manager" value="<?php echo esc_attr(get_option('p_google_tag_manager')); ?>"/>
                <br /><span style="font-size: x-small;">Ideally if you have google analytics that should be put as property in Tag Manger.</span>
            </td>
          </tr>
        </table>
      </div>


      <div id="Parkit" class="tabcontent">
        <table class="form-table">
          <tr>
            <th scope="row">Turn Parkit On:</th>
            <td>
              <input type="hidden" name="p_parkit_on" value="0" />
              <input type="checkbox" name="p_parkit_on" value="1" <?= get_option('p_parkit_on') ? 'checked' : '' ?> /> On?
            </td>
          </tr>
          <tr>
            <th scope="row">Parkit Card:</th>
            <td>
              <?php $cards = project_parkit_cards(); ?>
              <select name="p_parkit_card">
                <?php foreach ($cards as $card => $file) { ?>
                <option value="<?= $card ?>" <?= get_option('p_parkit_card') == $card ? 'SELECTED' : '' ?>><?= $card ?></option>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <th scope="row">Parkit Image:</th>
            <td><input type="file" name="p_parkit_image"/>
              <br/>
                <?php
                echo esc_attr(get_option('p_parkit_image')); ?>
              <br/>
              <input type="checkbox" name="delete_p_parkit_image" value="remove"/>clear?
            </td>
          </tr>
        </table>
      </div>


        <div id="BrandingFooter" class="tabcontent">
            <table class="form-table">
                <tr>
                    <th scope="row">Show the branding bar:</th>
                    <td>
                        <input type="hidden" name="p_branding_bar_show" value="0" />
                        <input type="checkbox" name="p_branding_bar_show" value="1" <?= get_option('p_branding_bar_show') ? 'checked' : '' ?> /> Show?
                    </td>
                </tr>
            </table>
        </div>



      <?php
        submit_button(); ?>
      <style>
          tr {
              vertical-align: top;
          }

          label {
              display: flex;
              width: auto;
          }

          input {
              display: inline-flex;
          }

          /* Style the tab */
          .tab {
              overflow: hidden;
          }

          /* Style the buttons that are used to open the tab content */
          .tab button {
              background-color: transparent;
              float: left;
              border: none;
              outline: none;
              cursor: pointer;
              padding: 14px 16px;
              font-size: 1rem;
              position: relative;
              box-shadow: none;
              display: inline-block;
          }

          /* Change background color of buttons on hover */
          .tab button:hover {
              background-color: #ccc;
              color: #ffff;
              animation: fadeEffect 1s;
          }

          /* Create an active/current tablink class */
          .tab button.active {
              background-color: #ccc;
              color: #ffff;
          }

          /* Style the tab content */
          .tabcontent {
              display: none;
              padding: 6px 12px;
              -webkit-animation: fadeEffect 1s;
              animation: fadeEffect 1s;
          }

          /* Fade in tabs */
          @-webkit-keyframes fadeEffect {
              from {
                  opacity: 0;
              }
              to {
                  opacity: 1;
              }
          }

          @keyframes fadeEffect {
              from {
                  opacity: 0;
              }
              to {
                  opacity: 1;
              }
          }

      </style>
      <script>

        function openSettings (settings) {
          // Declare all variables
          var i, tabcontent, tablinks

          // Get all elements with class="tabcontent" and hide them
          tabcontent = document.getElementsByClassName('tabcontent')
          for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = 'none'
          }

          // Get all elements with class="tablinks" and remove the class "active"
          tablinks = document.getElementsByClassName('tablinks')
          for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(' active', '')
          }

          // Show the current tab, and add an "active" class to the button that opened the tab
          document.getElementById(settings).style.display = 'block'
          document.getElementById('tab-' + settings).className += ' active'
        }

        document.addEventListener('DOMContentLoaded', function () {
          // Get the element with id="defaultOpen" and click on it
          document.getElementById('tab-Branding').click()
        })

      </script>
    </form>
  </div>
<?php
} ?>
