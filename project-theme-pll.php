<?php



/**
 * Init Polylang Theme Translation plugin.
 */
add_action('init', 'project_process_polylang_theme_translation');

function project_process_polylang_theme_translation()
{
    if (project_is_polylang_page()) {
        if (current_user_can('manage_options')) {
            // Let the show begin.

            $strings = [];
            // First get all the lazy block templates.
            $templates = glob(get_template_directory()."/lazy-blocks/*/*-template.php");
            foreach ($templates as $template) {
                $file_content = file_get_contents($template);
                $matches = [];
                preg_match_all("/{{#pll\s*'([a-z-]+)'\s*}}/", $file_content, $matches);
                if ($matches[0]) {
                    $strings = array_merge($strings, $matches[1]);
                }
            }
            foreach ($strings as $string) {
                pll_register_string($string, $string, 'project');
            }
        }
    }
}




/**
 * Check polylang string settings page.
 * @return bool
 */
function project_is_polylang_page()
{
    global $pagenow;

    if (is_admin() && isset($_GET['page']) && !empty($pagenow)) {
        if ($pagenow === 'options-general.php' && $_GET['page'] === 'mlang' && isset($_GET['tab']) && $_GET['tab'] === 'strings') {
            // wp-admin/options-general.php?page=mlang&tab=strings
            return true;
        } elseif ($pagenow === 'admin.php' && $_GET['page'] === 'mlang_strings') {
            // wp-admin/admin.php?page=mlang_strings
            return true;
        }
    }
    return false;
}
