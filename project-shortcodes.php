<?php

/**
 * Add a shortcode to do the head part of google tag manager
 *
 * @param $params
 * @param $content
 *
 * @return string
 */
function project_google_tag_manager_head_shortcode($params, $content): string
{
    $out = '';
    $gtm_id = get_option('p_google_tag_manager');

    if ($gtm_id)
    {
        $out = "
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-".htmlentities($gtm_id)."');
        </script>
        <!-- End Google Tag Manager -->
        ";

    }
    return $out;
}
add_shortcode('google_tag_manager_head', 'project_google_tag_manager_head_shortcode');

add_action('moove_gdpr_third_party_header_assets','project_moove_gdpr_third_party_header_assets');
function project_moove_gdpr_third_party_header_assets( $scripts ) {
    $scripts .= project_google_tag_manager_head_shortcode(null, null);
    return $scripts;
}



/**
 * Add a shortcode to do the body part of google tag manager
 *
 * @param $params
 * @param $content
 *
 * @return string
 */
function project_google_tag_manager_body_shortcode($params, $content): string
{
    $out = '';
    $gtm_id = get_option('p_google_tag_manager');

    if ($gtm_id)
    {
        $out = '
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-'.htmlentities($gtm_id).'"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        ';
    }
    return $out;
}
add_shortcode('google_tag_manager_body', 'project_google_tag_manager_body_shortcode');

function project_force_thirdparty_cookies_gdpr()
{
    $moove_gdpr_plugin_settings = get_option('moove_gdpr_plugin_settings');
    if (is_array($moove_gdpr_plugin_settings)) {
        $moove_gdpr_plugin_settings['moove_gdpr_third_party_cookies_enable_first_visit'] = 0;
        $moove_gdpr_plugin_settings['moove_gdpr_third_party_cookies_enable'] = 1;
        update_option('moove_gdpr_plugin_settings', $moove_gdpr_plugin_settings);
    }
}
add_action('init', 'project_force_thirdparty_cookies_gdpr');


