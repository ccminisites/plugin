<?php

function cptui_register_my_cpts() {

    /**
     * Post Type: Forms.
     */

    $labels = [
      "name" => __( "Forms", "project" ),
      "singular_name" => __( "Form", "project" ),
      "menu_name" => __( "My Forms", "project" ),
      "all_items" => __( "All Forms", "project" ),
      "add_new" => __( "Add new", "project" ),
      "add_new_item" => __( "Add new Form", "project" ),
      "edit_item" => __( "Edit Form", "project" ),
      "new_item" => __( "New Form", "project" ),
      "view_item" => __( "View Form", "project" ),
      "view_items" => __( "View Forms", "project" ),
      "search_items" => __( "Search Forms", "project" ),
      "not_found" => __( "No Forms found", "project" ),
      "not_found_in_trash" => __( "No Forms found in trash", "project" ),
      "parent" => __( "Parent Form:", "project" ),
      "featured_image" => __( "Featured image for this Form", "project" ),
      "set_featured_image" => __( "Set featured image for this Form", "project" ),
      "remove_featured_image" => __( "Remove featured image for this Form", "project" ),
      "use_featured_image" => __( "Use as featured image for this Form", "project" ),
      "archives" => __( "Form archives", "project" ),
      "insert_into_item" => __( "Insert into Form", "project" ),
      "uploaded_to_this_item" => __( "Upload to this Form", "project" ),
      "filter_items_list" => __( "Filter Forms list", "project" ),
      "items_list_navigation" => __( "Forms list navigation", "project" ),
      "items_list" => __( "Forms list", "project" ),
      "attributes" => __( "Forms attributes", "project" ),
      "name_admin_bar" => __( "Form", "project" ),
      "item_published" => __( "Form published", "project" ),
      "item_published_privately" => __( "Form published privately.", "project" ),
      "item_reverted_to_draft" => __( "Form reverted to draft.", "project" ),
      "item_scheduled" => __( "Form scheduled", "project" ),
      "item_updated" => __( "Form updated.", "project" ),
      "parent_item_colon" => __( "Parent Form:", "project" ),
    ];

    $args = [
      "label" => __( "Forms", "project" ),
      "labels" => $labels,
      "description" => "",
      "public" => false,
      "publicly_queryable" => false,
      "show_ui" => true,
      "show_in_rest" => false,
      "rest_base" => "",
      "rest_controller_class" => "WP_REST_Posts_Controller",
      "has_archive" => false,
      "show_in_menu" => true,
      "show_in_nav_menus" => false,
      "delete_with_user" => false,
      "exclude_from_search" => true,
      "capability_type" => "post",
      "map_meta_cap" => true,
      "hierarchical" => false,
      "rewrite" => false,
      "query_var" => false,
      "menu_icon" => "dashicons-feedback",
      "supports" => [ "title" ],
      "show_in_graphql" => false,
    ];

    register_post_type( "form", $args );

    /**
     * Post Type: Form Responses.
     */

    $labels = [
      "name" => __( "Form Responses", "project" ),
      "singular_name" => __( "Form Response", "project" ),
      "menu_name" => __( "My Form Responses", "project" ),
      "all_items" => __( "All Form Responses", "project" ),
      "add_new" => __( "Add new", "project" ),
      "add_new_item" => __( "Add new Form Response", "project" ),
      "edit_item" => __( "Edit Form Response", "project" ),
      "new_item" => __( "New Form Response", "project" ),
      "view_item" => __( "View Form Response", "project" ),
      "view_items" => __( "View Form Responses", "project" ),
      "search_items" => __( "Search Form Responses", "project" ),
      "not_found" => __( "No Form Responses found", "project" ),
      "not_found_in_trash" => __( "No Form Responses found in trash", "project" ),
      "parent" => __( "Parent Form Response:", "project" ),
      "featured_image" => __( "Featured image for this Form Response", "project" ),
      "set_featured_image" => __( "Set featured image for this Form Response", "project" ),
      "remove_featured_image" => __( "Remove featured image for this Form Response", "project" ),
      "use_featured_image" => __( "Use as featured image for this Form Response", "project" ),
      "archives" => __( "Form Response archives", "project" ),
      "insert_into_item" => __( "Insert into Form Response", "project" ),
      "uploaded_to_this_item" => __( "Upload to this Form Response", "project" ),
      "filter_items_list" => __( "Filter Form Responses list", "project" ),
      "items_list_navigation" => __( "Form Responses list navigation", "project" ),
      "items_list" => __( "Form Responses list", "project" ),
      "attributes" => __( "Form Responses attributes", "project" ),
      "name_admin_bar" => __( "Form Response", "project" ),
      "item_published" => __( "Form Response published", "project" ),
      "item_published_privately" => __( "Form Response published privately.", "project" ),
      "item_reverted_to_draft" => __( "Form Response reverted to draft.", "project" ),
      "item_scheduled" => __( "Form Response scheduled", "project" ),
      "item_updated" => __( "Form Response updated.", "project" ),
      "parent_item_colon" => __( "Parent Form Response:", "project" ),
    ];

    $args = [
      "label" => __( "Form Responses", "project" ),
      "labels" => $labels,
      "description" => "",
      "public" => false,
      "publicly_queryable" => false,
      "show_ui" => false,
      "show_in_rest" => false,
      "rest_base" => "",
      "rest_controller_class" => "WP_REST_Posts_Controller",
      "has_archive" => false,
      "show_in_menu" => false,
      "show_in_nav_menus" => false,
      "delete_with_user" => false,
      "exclude_from_search" => true,
      "capability_type" => "post",
      "map_meta_cap" => true,
      "hierarchical" => false,
      "rewrite" => false,
      "query_var" => false,
      "supports" => [ "title" ],
      "show_in_graphql" => false,
    ];

    register_post_type( "form-response", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );

