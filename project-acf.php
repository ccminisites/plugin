<?php

if( function_exists('acf_add_local_field_group') ):

    $fields = array(
      'key' => 'group_621740f8b5d67',
      'title' => 'Form Details',
      'fields' => array(
        array(
          'key' => 'field_62174a98c691b',
          'label' => 'The Form',
          'name' => 'the_form',
          'type' => 'formbuilder',
          'instructions' => 'Create your form using drag n drop elements here.',
          'required' => 1,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'font_size' => 14,
        ),
        array(
          'key' => 'field_621755d46cade',
          'label' => 'Email Responses To',
          'name' => 'email_responses_to',
          'type' => 'email',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
        ),
        array(
          'key' => 'field_62176bdb82b55',
          'label' => 'Thank You Message',
          'name' => 'the_thankyou',
          'type' => 'wysiwyg',
          'instructions' => 'This is the message that is displayed when the form has been successfully filled in.',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '<h2>Thank You</h2>

<p>
We will be in touch with you very shortly.
</p>',
          'tabs' => 'all',
          'toolbar' => 'basic',
          'media_upload' => 1,
          'delay' => 0,
        ),
      ),
      'location' => array(
        array(
          array(
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'form',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => array(
        0 => 'permalink',
        1 => 'the_content',
        2 => 'excerpt',
        3 => 'discussion',
        4 => 'comments',
        5 => 'revisions',
        6 => 'slug',
        7 => 'author',
        8 => 'format',
        9 => 'page_attributes',
        10 => 'featured_image',
        11 => 'categories',
        12 => 'tags',
        13 => 'send-trackbacks',
      ),
      'active' => true,
      'description' => '',
      'show_in_rest' => 0,
    );

    //die(var_dump(json_encode($fields)));

    acf_add_local_field_group($fields);
endif;