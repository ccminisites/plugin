<?php
/*
Plugin Name: Project
Plugin URI: https://www.contentcoffee.com
Description: Base Project Plugin for CC Minisites
Author: Content and Coffee
Version: 1.0
*/

add_filter('auto_update_plugin', '__return_false');
add_filter('auto_update_theme', '__return_false');

include_once 'project-theme-pll.php';
include_once 'project-settings.php';
include_once 'project-forms.php';
include_once 'project-cpt.php';
include_once 'project-acf.php';
include_once 'project-shortcodes.php';
