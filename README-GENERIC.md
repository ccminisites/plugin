# Wordpress Project Plugin

Extremely simple wordpress plugin to get your started.

## Install

Download and copy the inner contents to a directory called "project" in your plugins folder.

or

Copy this into an existing folder and edit the project.php to match the name.

## Contribute

Clone this, make changes, and make a pull request.
